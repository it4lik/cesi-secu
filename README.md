# CESI Security

* [Cours](./cours)
* [TPs](./tp)

# Déroulement

Beaucoup de notions ont été abordés sur ces 3 jours. Le but était de fournir aux participants un regard avisé sur ce qu'est réellement le monde de la sécurité et du hacking. Ont été abordés des sujets techniques (en particulier lors des TPs) mais aussi parfois des sujets plus engageants politiquement de façon relativement succincte (place du hacker dans l'IT/la société, neutralité du web, anonymat en ligne, etc.) 

## Cours

### 1er jour

* notions générales autour de la sécurité
  * [voir le diapo associé](./cours/slides/day1.html)

### 2ème jour

* introduction à la crypto
  * encodage
  * hashing
  * chiffrement
    * appréhension des mathématiques derrière le chiffrement RSA

### 3ème jour

* anonymat, chiffrement et vie privée sur internet
  * notions d'anonymat (navigation privée, proxies, VPNs, Tor, DoH et DoT)
  * fonctionnement du réseau Tor
  * cas d'utilisation du réseau Tor
  * sensibilisation aux utilisations légitimes de Tor
* écosystème sécurité en ligne
  * visite et sensibilisation de plusieurs plateformes en ligne liées à la sécurité (CVEdetails, exploit-db, hackerone, virustotal, etc.)
  * Kali Linux
    * quoi qu'est-ce
    * quand et comment l'utiliser
    * pertinence

## TP

* [Exploitation du protocole ARP](tp/mitm/)
  * ARP spoofing
  * man-in-the-middle
  * DNS spoofing
* [Sécurisation d'un serveur Web](./tp/webserver/)
* [Exploitation de vulnérabilités et mauvaises configurations](./tp/vuln/)
  * DoS Apache avec Slow Loris
  * Exploitation mauvaise configuration Redis
  * Exploitation et privilege escalation sur un serveur Web Apache