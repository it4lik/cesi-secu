# Cours CESI Security

* Slides
  * [Intro sécurité](./slides/day1.html)

# Ideas

* VPN
* Tor (darknet, deepweb, toussa)
* Vulnerability exploitation
* Misconfiguration exploitation
* Firewalling avancé
