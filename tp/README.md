# CESI Security - TP

* [ARP spoofing, Man-in-the-middle, DNS spoofing](./mitm/)
* [Web Server hardening](./webserver/)
* [Vulnérabilités connues et mauvaises configurations](./vuln)

## Prérequis

Il est **FORTEMENT** recommandé d'utiliser des machines GNU/Linux pour les TPs.  

Pour les parties nécessitant un Linux léger ou orienté serveur, préférez [**CentOS 7 en version minimale**](http://miroir.univ-lorraine.fr/centos/7.7.1908/isos/x86_64/CentOS-7-x86_64-Minimal-1908.iso).

Pour avoir une machine attaquante facile à équiper :
* Fedora : basé sur RedHat, orienté développement, très fourni en packages
* Kali Linux : basé sur Debian, orienté sécurité
* Debian : simple, customisable, communautaire

