# ARP man-in-the-middle

OS recommandé : CentOS7 (avec VirtualBox par exemple)

Vous aurez aussi besoin de Wireshark sur votre machine hôte. Il permet d'ouvrir les fichiers `.pcap` capturés avec `tcpdump`.

- [ARP man-in-the-middle](#arp-man-in-the-middle)
- [Intro : ARP](#intro--arp)
  - [Pourquoi ARP ?](#pourquoi-arp)
  - [Concept](#concept)
  - [En pratique](#en-pratique)
- [ARP spoofing](#arp-spoofing)
  - [Exemple](#exemple)
    - [Situation initiale](#situation-initiale)
    - [Trame malformée](#trame-malformée)
  - [ARP man-in-the-middle](#arp-man-in-the-middle-1)
- [Mise en place du TP](#mise-en-place-du-tp)
  - [Prérequis](#prérequis)
  - [Liens utiles](#liens-utiles)
  - [But du TP](#but-du-tp)
  - [Bonus : DNS spoofing](#bonus--dns-spoofing)

# Intro : ARP

## Pourquoi ARP ? 

ARP est un protocole réseau permettant de faire le lien entre des adresses IP et des adresses MAC.

Ce protocole est nécessaire car :
* on ne peut communiquer sur un réseau qu'en envoyant des **trames Ethernet**
  * les paquets IP sont encapsulés dans des trames Ethernet
* les **trames Ethernet possèdent** (entre autres) **une adresse MAC source et une adresse MAC de destination**
  * il est donc nécessaire de connaître l'adresse MAC de la machine avec laquelle on veut discuter
* c'est **l'adresse IP d'une machine que nous connaissons avant de communiquer avec**
  * il nous faut un moyen de trouver son adresse MAC

Ainsi, en connaissant l'adresse IP d'une machine, il est possible, grâce à ARP, de demander sur le réseau à qui appartient l'IP en question.

> ARP et Ethernet sont des protocoles de niveau 2 : ils ne circulent que au sein d'un LAN, et sont incapables de sortir d'un LAN.

## Concept

Le principe de base du protocole ARP repose sur un système de question/réponse :
* une machine A peut demander sur le réseau à qui appartient une IP donnée
* la machine B concernée par le message, celle qui porte l'IP demandée, répond à la machine A
* les trames envoyées ayant toutes une adresse MAC source (celle de la machine qui envoie le message)

Les deux principaux messages ARP sont :

| Trame ARP   | MAC Source | MAC Destination | IP Source | IP Destination | Signification                                                                                            |
|-------------|------------|-----------------|-----------|----------------|----------------------------------------------------------------------------------------------------------|
| ARP Request | MAC A      | Broadcast       | IP A      | IP B           | La machine A demande à tout le réseau quelle est la MAC de la machine qui porte l'IP B                   |
| ARP Reply   | MAC B      | MAC A           | IP B      | IP A           | Grâce à la MAC source présent dans l'ARP request, la machine B peut répondre directement à la machine A. |

Une fois que la machine A a réceptionné le message ARP Reply, elle connaît l'adresse MAC de la machine B : elle est dans l'addresse MAC source de l'ARP reply.

> L'adresse MAC de broadcast est l'adresse `ff:ff:ff:ff:ff:ff`. Envoyer une trame à cette adresse permet d'envoyer une trame que toutes les machines du réseau recevront.

Une fois que les machines connaissent l'adresse MAC de l'autre, **elles l'inscrivent dans leur table ARP.**

La table ARP contient la liste des adresses MAC connues, associées à l'IP. Les entrées de la table ARP sont remplies grâce au système d'ARP request/ARP reply.

## En pratique

Supposons deux clients :

| Machine | IP             | MAC                 |
|---------|----------------|---------------------|
| `node1` | `10.1.1.10/24` | `10:10:10:10:10:10` |
| `node2` | `10.1.1.20/24` | `20:20:20:20:20:20` |

Supposons qu'ils peuvent communiquer, mais qu'ils ne l'ont jamais fait auparavant : **leur table est vierge**.
Etudions le cas où `node1` envoie un message à `node2`, admettons, un ping :

Etape | Description                                                                                | Type | MAC Source          | MAC Destination     | IP Source   | IP destination | Message                                  |
------|--------------------------------------------------------------------------------------------|------|---------------------|---------------------|-------------|----------------|------------------------------------------|
**1** | `node1` tape `ping 10.1.1.20`                                                              | x    | x                   | x                   | x           | x              | x                                        |
**2** | `node1` envoie un ARP request pour connaître la MAC de `node2`                             | ARP  | `10:10:10:10:10:10` | `ff:ff:ff:ff:ff:ff` | `10.1.1.10` | `10.1.1.20`    | "Who has 10.10.10.20 ? Tell 10.10.10.10"
**3** | `node2` reçoit la trame ARP request  de `node1` et ajoute la MAC de `node1` à sa table ARP | X    | x                   | x                   | x           | x              | x
**4** | `node2` envoie un ARP reply à `node1`                                                      | ARP  | `20:20:20:20:20:20` | `10:10:10:10:10:10` | `10.1.1.20` | `10.1.1.10`    | "10.10.10.20 is at 20:20:20:20:20:20"
**5** | `node1` reçoit la trame ARP reply de `node2` et ajoute la MAC de `node2` à sa table ARP    | X    | x                   | x                   | x           | x              | x
**6** | le ping de `node1` peut partir                                                             | ICMP | `10:10:10:10:10:10` | `20:20:20:20:20:20` | `10.1.1.10` | `10.1.1.20`    | x

Une fois l'échange ARP terminé, les machines `node1` et `node2` peuvent communiquer car connaissant leurs adresses MAC respectives.

# ARP spoofing

**L'ARP spoofing** ou **ARP cache poisining** correspond à **l'abus du protocole ARP** dans le but d'une **usurpation d'identité** sur le réseau.

Il est possible, en envoyant des trames malformées d'ajouter de fausses informations dans la table ARP des autres machines du réseau.

## Exemple

### Situation initiale

| Machine | IP | MAC |
|---------|---------|--- |
| victime | `10.1.1.10` | `10:10:10:10:10:10`
| passerelle | `10.1.1.1` | `01:01:01:01:01:01`
| attaquant | `10.1.1.20` | `20:20:20:20:20:20`

### Trame malformée

Prenons le cas où l'attaquant veut faire croire à la victime qu'il est la passerelle : 

| Trame       | Description                                       | MAC Source          | Mac Destination      | IP Source      | IP Destination |
|-------------|---------------------------------------------------|---------------------|----------------------|----------------|----------------|
| ARP request | L'attaquant veut connaître la MAC de la victime   | `20:20:20:20:20:20` | `ff:ff:ff:ff:ff:ff`  | `10.1.1.20`    | `10.1.1.10`    |
| ARP request | L'attaquant empoisonne la table ARP de la victime | `20:20:20:20:20:20` | ` 10:10:10:10:10:10` | **`10.1.1.1`** | `10.1.1.10`    |

**L'IP source envoyée par l'attaquant est une fausse information.**

La victime va alors associer l'adresse IP `10.1.1.1` à la MAC `20:20:20:20:20:20`, autrement dit, l'IP de la passerelle à la MAC de l'attaquant.

## ARP man-in-the-middle

Un cas typique est le suivant : 
* une victime, un attaquant, et un routeur
* le routeur permet d'accéder au WAN (accès internet)
* l'attaquant utilise l'ARP spoofing pour faire croire : 
  * à la victime qu'il est la passerelle (le routeur)
  * au routeur qu'il est la victime
* l'attaquant envoie des trames en permanence pour maintenir l'attaque en place

**Ainsi, tout le trafic entre le routeur et la victime passera par l'attaquant.**

# Mise en place du TP 

## Prérequis 

3 machines CentOS7 :
* victime
* attaquant
* passerelle

Commandes utiles :
```bash
# Afficher la liste des interfaces de la machine (voir IP et MAC)
ip address
ip a # plus court

# Afficher la table ARP de la machine
ip neigh show
ip n s # plus court

# Afficher la table de routage de la machine
ip route show
ip r s # plus court

# Envoyer un message simple ICMP echo request
ping <IP_address>
ping 10.1.1.1

# Installer des paquets
sudo yum install <PAQUET> 
sudo yum install traceroute tcpdump bind-utils # recommandé pour ce TP

# Capturer un trafic réseau
sudo tcpdump -i <INTERFACE>
sudo tcpdump -i eth0 # capture le trafic de l'interface eth0
sudo tcpdump -i eth0 -n -nn # affiche les numéros de protocoles plutôt que les noms 
sudo tcpdump -i eth0 -w capture.pcap # enregistre la capture dans un fichier au format .pcap (ouvrables avec Wireshark)
sudo tcpdump -i eth0 arp # ne capture que le trafic ARP
sudo tcpdump -i eth0 not port 22 # exclut le trafic sur le port 22
# Exemple de capture complète
sudo tcpdump -i eth0 -n -nn -w capture.pcap arp 

# Afficher les intermédiaires pour joindre une destination (niveau IP, niveau 3)
traceroute <DESTINATION>
traceroute 10.1.1.1
```

## Liens utiles

* [ARP spoofing avec `arping`](https://sandilands.info/sgordon/arp-spoofing-on-wired-lan)
* ARP spoofing avec Scapy (google it)
* il existe d'autres outils pour mettre cette attaque en place

## But du TP

Mettre en place un Man-in-the-middle : la machine attaquant doit se mettre entre la victime et la passerelle 
* passez du temps à afficher les tables ARP et visualiser les trames avec Wireshark
* prouvez que vous êtes capables d'empoisonner la table ARP la victime avec ce que vous voulez

Aidez-vous des liens et commandes au dessus.

**Faites des recherches internet, n'hésitez pas à poser des questions.**

## Bonus : DNS spoofing

Il est possible, une fois un Man-in-the-middle ARP établi, d'émettre des fausses réponses DNS vers la victime afin de la rediriger vers de faux sites Web. 

Objectif : mettre en place du DNS spoofing.
