# Exploitation de vulns connues et mauvaises configurations

<!-- vim-markdown-toc GitLab -->

* [I. Slow Loris](#i-slow-loris)
    * [Intro](#intro)
    * [Mise en place du lab](#mise-en-place-du-lab)
    * [Mise en place de l'attaque](#mise-en-place-de-lattaque)
        * [Footprinting](#footprinting)
        * [L'attaque](#lattaque)
* [II. Misconfiguration Redis](#ii-misconfiguration-redis)
* [III. Vulnerable wordpress](#iii-vulnerable-wordpress)

<!-- vim-markdown-toc -->

# I. Slow Loris

## Intro

Slow Loris est une attaque toujours efficace pour cause un déni de service sur une application Web. L'application repose non pas sur un flood de la machine, mais d'une surcharge du nombre de connexions simultanées possibles à un serveur.

Cette attaque causant un DoS, simple mais effiace, est réalisable avec une simple machine, et ne nécessite pas une grosse quantité de données comme beaucoup d'autres attaques de ce genre. Elle ne dépend pas non plus de la puissance de la machine attaquée.

> DoS un site web avec une raspberry Pi, trkl

**Je vous invite fortement à vous documenter plus en profondeur sur cette attaque par vous-même avant de continuer.**

## Mise en place du lab

* Mettre en place deux machines
  * un serveur web, la victime
  * un attaquant (l'attaquant peut être votre machine hôte)
* Installer le serveur Web
  * installer Apache sur le serveur web
  * lancer apache (configuration par défaut)
  * vérifier le bon fonctionnement (avec un navigater web)

## Mise en place de l'attaque

### Footprinting

**AVANT** de réaliser l'attaque, vous pouvez vous prêter au jeu du hacker et plus particulièrement du *footprinting* : la récolte d'informations sur une cible.

Depuis l'extérieur du serveur web, depuis votre hôte par exemple, récupérez :
* l'OS utilisé par le serveur web
* le nom du serveur web
* la version du serveur web

### L'attaque

Vous pouvez là aussi réaliser votre propre script. Le cas échéant, il existe là encore plusieurs outils qui permettent de réaliser une telle attaque :

```bash
# Récupération d'un script SlowLoris (allez jeter un oeil à la page github, qui est très complète)
$ git clone https://github.com/brannondorsey/SlowLoris

# Augmentation du nombre de fichiers ouvrables simultanément sur notre machine
$ ulimit -n 65536

# Exécution de l'attaque
$ cd SlowLoris
$ ./slowloris.py  --host <IP_VM> --port <PORT> --max-sockets 30000
```

**Vérifier que le service est tombé.**  

**Essayez de trouver une contre-mesure.**

# II. Misconfiguration Redis

Redis est une base de données clé/valeur. Elle ne contient pas de tables, mais siplement un ensemble de clé, possédant une valeur qui peut être de plusieurs types différents. Redis a déjà essuyé un certain nombre de vulnérabilité, mais un certain nombre de mauvaises configurations classiques peuvent être faillibles.

Histoire de rendre le truc plus fun, faisons le avec un Redis en dernière version.

---

Pour réaliser l'attaque :
* filez-moi vos clés SSH
* connectez vous au serveur

Vous arrivez sur une machine d'administration : la machine **attaquante**.

A votre disposition :
* commandes réseau habituelles 
  * `ip a`
  * `ip r s`
  * `ping`
  * `traceroute`
  * `nmap`
* les droits `root` avec `sudo`

---

1. **Footprinting :** La machine Redis à attaquer est `10.91.165.127` (testez que vous pouvez la `ping`)
2. **Footprinting :** Trouvez un max d'infos sur la machine (en priorité : sur quel port tourne Redis, et quelle version de Redis tourne)
3. **Exploitation :** Exploiter Redis ( = prendre possession du serveur Redis)

> Hint : pour l'exploitation, vous aurez besoin de l'outil `redis-cli` afin de vous connecter au Redis.

# III. Vulnerable wordpress

Ici, on va travailler avec une machine virtuelle prête à importer.

La machine qu'on va utiliser est disponible sur VulnHub : https://www.vulnhub.com/entry/mr-robot-1,151/

Le but est de :
* récupérer la VM
* lancer la VM
* exploiter la VM

Il est possible de devenir *root* sur cette machine. Le point d'entrée est une application Web (utilisez des outils de scan pour trouver les applications en écoute sur la cible).

Tools utiles : 
* `nmap`
* `nikto`
* `wpscan`
* `metasploit`
* your brain
