# Sécurisation Serveur web

- [Sécurisation Serveur web](#sécurisation-serveur-web)
  - [Prérequis](#prérequis)
  - [Etapes préliminaires](#etapes-préliminaires)
  - [Machine 1](#machine-1)
  - [Machine 2](#machine-2)
  - [Aller plus loin](#aller-plus-loin)

## Prérequis

Deux machines CentOS7 qui peuvent se ping. 
* machine1 : serveur web
* machine2 : reverse proxy

But du TP : 
```
Client ---HTTPS----> Reverse Proxy ----HTTP----> Serveur Web
```

**IL FAUT DESACTIVER SELinux SUR LES DEUX MACHINES POUR REALISER LE TP**.

## Etapes préliminaires

Mettre en place :
* OS de base + conf réseau
* **accès à distance** (configuration SSH)
  * **configuration renforcée d'OpenSSH** (faites des recherches,  [ce document de l'ANSSI](https://www.ssi.gouv.fr/uploads/2015/09/NT_OpenSSH_en.pdf) est un bon point de départ, mais il est un peu vieux maintenant donc un peu obsolète sur certains points)
  * **gestion d'utilisateurs et de clés** pour l'authentification
  * pas d'authentification par mot de passe
* **fail2ban**
  * pour sécuriser l'accès SSH
* **firewall**
  * autorise l'accès SSH
  * drop le reste

## Machine 1

**Serveur web** : 
* installer Apache
* mettre en place une page HTML de test (ou un site comme wordpress)

**Checklist sécurité** :
* le serveur Apache doit avoir un utilisateur dédié
* la configuration Apache doit être renforcée
* vous devez connaître l'emplacement des logs
* le serveur Apache ne doit pas être joignable par les clients
* ajuster la configuration du firewall pour que le serveur Web apache ne soit joignable QUE depuis l'IP du reverse proxy sur le port 80

## Machine 2

**Reverse proxy** :
* installer NGINX
* configurer NGINX pour rediriger vers le serveur Apache

**Checklist sécurité** :
* le serveur NGINX doit avoir un utilisateur dédié
* configurer un certificat et une clé pour du HTTPs
  * le serveur redirige tout le trafic HTTP vers HTTPS
* vous devez connaître l'emplacement des logs
* la configuration de NGINX doit être renforcée en terme de sécurité
* ajuster la configuraiton du firewall pour autoriser les connexions sur les port 80 et 443.
* ajuster la configuration de fail2ban pour qu'il agisse sur les logs de Apache

## Aller plus loin

* **métrologie & alertes**
  * pouvoir avoir des métriques sur l'utilisation de la machine et de l'app web
  * pouvoir avoir des alertes automatisées en cas de dépassement d'un certain seuil (mails, slack, ...)
  * je vous recommande [netdata](https://github.com/netdata/netdata)
* utiliser un WAF sur le serveur Web : **Web Application Firewall**
  * [modsecurity](http://modsecurity.org/) est très reconnu pour Apache
  * un WAF permet de filtrer les requêtes effectuées à l'application Web en foncion de leur contenu
* **conteneuriser** l'application
  * augmente le niveau de sécurité (isolation) de l'application en la plaçant dans un conteneur
  * vous pouvez utiliser [Docker](https://docs.docker.com/install/)
* mettre en place une **sauvegarde automatisée** de l'application Web
  * je vous conseille [borgbackup](https://www.borgbackup.org/)
